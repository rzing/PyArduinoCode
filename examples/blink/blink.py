from pymata_aio.pymata3 import PyMata3
from pymata_aio.constants import Constants

board = PyMata3()

LED_PIN = 2

board.set_pin_mode(LED_PIN, Constants.OUTPUT)

while True:
    board.digital_write(LED_PIN, 1)

    board.sleep(1)

    board.digital_write(LED_PIN, 0)

    board.sleep(1)
