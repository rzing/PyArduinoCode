from pymata_aio.pymata3 import PyMata3
from pymata_aio.constants import Constants

board = PyMata3()

TRIG_PIN = 2
ECHO_PIN = 3

board.set_pin_mode(TRIG_PIN, Constants.OUTPUT)
board.set_pin_mode(ECHO_PIN, Constants.INPUT)

def distance_ping(data):
    distance = data[1]

    print(str(distance) + ' units')

board.sonar_config(TRIG_PIN, ECHO_PIN, distance_ping)

while True:
    board.sleep(.1)
