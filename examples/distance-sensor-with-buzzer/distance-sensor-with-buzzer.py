from pymata_aio.pymata3 import PyMata3
from pymata_aio.constants import Constants

board = PyMata3()

TRIG_PIN = 2
ECHO_PIN = 3
BUZZER_PIN = 5

board.set_pin_mode(TRIG_PIN, Constants.OUTPUT)
board.set_pin_mode(ECHO_PIN, Constants.INPUT)
board.set_pin_mode(BUZZER_PIN, Constants.PWM)

def distance_ping(data):
    distance = data[1]

    print(str(distance) + ' units')

    if distance > 1 and distance < 100:
        frequency = distance * 20
    elif distance > 100:
        frequency = 2500

    try:
        board.play_tone(BUZZER_PIN, Constants.TONE_TONE, frequency, 1500)
    except:
        pass

board.sonar_config(TRIG_PIN, ECHO_PIN, distance_ping)

while True:
    board.sleep(.1)
