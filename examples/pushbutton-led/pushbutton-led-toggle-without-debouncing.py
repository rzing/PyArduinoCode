from pymata_aio.pymata3 import PyMata3
from pymata_aio.constants import Constants

board = PyMata3()

LED_PIN = 12
BUTTON_PIN = 11

board.set_pin_mode(LED_PIN, Constants.OUTPUT)
board.set_pin_mode(BUTTON_PIN, Constants.INPUT)

led_value = 0

while True:
    current_value = board.digital_read(BUTTON_PIN)

    # If the value we get is 1, that is, the button is pressed...
    if current_value == 1:

        # Then change the LED to its other state - on if it's off, and off if it's on.
        if led_value == 0:
            board.digital_write(LED_PIN, 1)
            led_value = 1
        else:
            board.digital_write(LED_PIN, 0)
            led_value = 0
