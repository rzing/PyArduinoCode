from pymata_aio.pymata3 import PyMata3
from pymata_aio.constants import Constants
from time import time

board = PyMata3()

LED_PIN = 12
BUTTON_PIN = 11

board.set_pin_mode(LED_PIN, Constants.OUTPUT)
board.set_pin_mode(BUTTON_PIN, Constants.INPUT)

led_value = 0
current_button_value = 0
last_button_value = 0
last_debounce_time = int(time() * 1000)
DEBOUNCE_DELAY = 50

board.digital_write(LED_PIN, led_value)

def debounce(current_unconfirmed_value):
    global current_button_value
    global last_button_value
    global last_debounce_time

    # We don't know if the button has truly changed yet
    button_has_changed = False

    # Here we check if it is changed, but this could be due
    # to both real pressing, and noise from the bounce.

    if current_unconfirmed_value != last_button_value:

        # Either way, we update the last debounce time.
        last_debounce_time = int(time() * 1000)

    # Here we check if it has been long enough since the last reading.
    if ((int(time() * 1000)) - last_debounce_time) > DEBOUNCE_DELAY:

        # Remember, current button state holds the value we confirmed
        # the button to have *actually* had - without noise. So we check
        # whether *that* value is different from this one. If it is...

        if current_unconfirmed_value != current_button_value:
            # ...it has to be the result of a *real* change in state, rather than noise,
            # since this reading is far enough away from the last one.

            current_button_value = current_unconfirmed_value # This means the value becomes *CONFIRMED*

            # We mark the variable as well

            button_has_changed = True

    # Then we update the last value so we have it to compare to, the next time we do this
    last_button_value = current_unconfirmed_value

    return button_has_changed

while True:
    current_unconfirmed_value = board.digital_read(BUTTON_PIN)

    button_has_changed = debounce(current_unconfirmed_value)

    # We *have* to check if the button has changed - otherwhise,
    # if someone is holding the button down, we will mistakenly
    # alternate between on and off, which is not what we want.
    
    if current_button_value == 1 and button_has_changed:
        if led_value == 0:
            board.digital_write(LED_PIN, 1)
            led_value = 1
        else:
            board.digital_write(LED_PIN, 0)
            led_value = 0
