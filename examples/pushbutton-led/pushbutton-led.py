from pymata_aio.pymata3 import PyMata3
from pymata_aio.constants import Constants

board = PyMata3()

LED_PIN = 12
BUTTON_PIN = 11

board.set_pin_mode(LED_PIN, Constants.OUTPUT)
board.set_pin_mode(BUTTON_PIN, Constants.INPUT)

while True:
    current_value = board.digital_read(BUTTON_PIN)

    board.digital_write(LED_PIN, current_value)
